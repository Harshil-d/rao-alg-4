file_path = "./tests.dat"

data = []

file = open(file_path, "r")
fileData = file.read().split("\n")

for line in fileData:
    line = line.replace("[","")
    line = line.replace("]", "")
    oneArray = []
    line = line.split(',')
    for oneData in line:
        oneArray.append(oneData)
    data.append(oneArray)

if len(data)%3 != 0:
    print("please Enter data")

for index in range(0, len(data), 3):
    dineOrder = data[index]
    takeAwayOrder = data[index+1]
    servedOrders = data[index+2]

    takeAwayOrderIndex = 0
    dineOrderIndex = 0

    isValid = True

    for s in range(len(servedOrders)):
        if s == 0:
            if (not servedOrders[s] == takeAwayOrder[0]):
                isValid = False
                print("data set ", (index/3)+1, " is invalid")
                break
            else:
                takeAwayOrderIndex += 1
                continue

        if len(takeAwayOrder) == takeAwayOrderIndex:
            if servedOrders[s] == dineOrder[dineOrderIndex]:
                dineOrderIndex += 1
                continue
            else:
                isValid = False
                print("data set ", (index/3)+1, " is invalid")
                break
        elif len(dineOrder) == dineOrderIndex:
            if servedOrders[s] == takeAwayOrder[takeAwayOrderIndex]:
                takeAwayOrderIndex += 1
                continue
            else:
                isValid = False
                print("data set ", (index/3)+1, " is invalid")
                break

        if (takeAwayOrder[takeAwayOrderIndex] > servedOrders[s-1] and dineOrder[dineOrderIndex] > servedOrders[s-1]) or (takeAwayOrder[takeAwayOrderIndex] < servedOrders[s-1] and dineOrder[dineOrderIndex] < servedOrders[s-1]):
            if takeAwayOrder[takeAwayOrderIndex] < dineOrder[dineOrderIndex]:
                if servedOrders[s] == takeAwayOrder[takeAwayOrderIndex]:
                    takeAwayOrderIndex += 1
                    continue
                else:
                    isValid = False
                    print("data set ", (index/3)+1, " is invalid")
                    break
            else:
                if servedOrders[s] == dineOrder[dineOrderIndex]:
                    dineOrderIndex += 1
                    continue
                else:
                    isValid = False
                    print("data set ", (index/3)+1, " is invalid")
                    break
        else:
            if takeAwayOrder[takeAwayOrderIndex] > dineOrder[dineOrderIndex]:
                if servedOrders[s] == takeAwayOrder[takeAwayOrderIndex]:
                    takeAwayOrderIndex += 1
                    continue
                else:
                    isValid = False
                    print("data set ", (index/3)+1, " is invalid")
                    break
            else:
                if servedOrders[s] == dineOrder[dineOrderIndex]:
                    dineOrderIndex += 1
                    continue
                else:
                    isValid = False
                    print("data set ", (index/3)+1, " is invalid")
                    break

    if isValid:
        print("data set ", (index/3)+1, " is valid")

